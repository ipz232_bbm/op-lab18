#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int summa(int arr[], int n) {
	if (n == -1) return 0;
	return arr[n] + summa(arr, n - 1);
}

void genArray(int* arr, int n) {
	for (int i = 0; i < n; i++) {
		arr[i] = rand() % 100;
		printf("%2d ", arr[i]);
	}
}

int main() {
	srand(time(NULL));

	int arr[10], n = 10;
	genArray(arr, n);

	int sum = summa(arr, n-1);
	printf("\nSumma = %d", sum);

	return 0;
}