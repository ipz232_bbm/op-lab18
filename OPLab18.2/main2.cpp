#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double resultWithRecursion(int n, int i) {
	if (i > n) return 0;
	return sqrt(i + resultWithRecursion(n, i + 1));
}

double resultWithoutRecursion(int n) {
	double result = 0;
	for (int i = n; i >= 1; i--) {
		result = sqrt(i + result);
	}
	return result;
}

int scanning() {
	int scan;
	do {
		printf("n (n > 0) = ");
		scanf("%d", &scan);
	} while (scan <= 0);
	return scan;
}

int main() {
	int n;
	n = scanning();

	double result = resultWithoutRecursion(n);
	printf("result without recursion = %lf\n", result);
	result = resultWithRecursion(n, 1);
	printf("result with recursion = %lf", result);

	return 0;
}