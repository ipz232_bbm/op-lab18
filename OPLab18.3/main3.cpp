#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int counterOfDigits(long long number) {
	if (!number) return 0;
	return 1 + counterOfDigits(number/10);
}

int main() {
	
	long long number;
	scanf("%llu", &number);
	int digits;

	if (number)
		digits = counterOfDigits(number);
	else digits = 1;

	printf("%d", digits);	

	return 0;
}